package it.robertoamato.web.config;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;

@Configuration
@ComponentScan(basePackages = "it.robertoamato.web")
@PropertySource( value = {"classpath:application.properties"})
public class RootApplicationContextConfig {

	private static Logger log = LoggerFactory.getLogger(RootApplicationContextConfig.class);
	
	private static final String SCRIPT_CREATE_SCHEMA_PATH = "db/sql/create-table.sql";
	private static final String SCRIPT_INSERT_DATA_PATH = "db/sql/insert-data.sql";
//	private static final String SCRIPT_CREATE_SCHEMA_PATH = "spring.database.path.create-schema-script";
//	private static final String SCRIPT_INSERT_DATA_PATH = "spring.database.path.insert-data-script";
	
	@Bean
	public DataSource dataSource() {
		log.info("dataSourece - init");
		EmbeddedDatabaseBuilder builder = new EmbeddedDatabaseBuilder();
		EmbeddedDatabase db = builder.setType(EmbeddedDatabaseType.HSQL)
									 .addScript(SCRIPT_CREATE_SCHEMA_PATH)
									 .addScript(SCRIPT_INSERT_DATA_PATH)
									 .build();
		return db;
	}

	@Bean
	public NamedParameterJdbcTemplate getJdbcTemplate() {
		log.info("getJdbcTemplate - START");
		return new NamedParameterJdbcTemplate(this.dataSource());
	}
}
