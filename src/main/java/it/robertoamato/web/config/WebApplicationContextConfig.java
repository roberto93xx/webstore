package it.robertoamato.web.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;
import org.springframework.web.servlet.view.InternalResourceViewResolver;
import org.springframework.web.servlet.view.JstlView;

@Configuration
@EnableWebMvc
@ComponentScan("it.robertoamato.web")
@PropertySource( value = {"classpath:application.properties"})
public class WebApplicationContextConfig extends WebMvcConfigurerAdapter {

	private static Logger log = LoggerFactory.getLogger(WebApplicationContextConfig.class);
	
	public static final String PREFIX_PATH = "springmvc.prefix.path.viewresolver"; 
	public static final String SUFFIX_VIEW = "springmvc.suffixview.viewresolver"; 
	
	@Autowired
	Environment environment;
	
	public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
		log.info("configureDefaultServletHandling - START");
		configurer.enable();
	}

	@Bean
	public InternalResourceViewResolver getInternalResourceViewResolver() {
		log.info("getInternalResourceViewResolver - START");
		InternalResourceViewResolver resolver = new InternalResourceViewResolver();
		resolver.setViewClass(JstlView.class);
//		resolver.setPrefix(this.environment.getProperty(PREFIX_PATH_VIEW));
//		resolver.setSuffix(this.environment.getProperty(SUFFIX_VIEW));
		resolver.setPrefix("/WEB-INF/jsp/");
		resolver.setSuffix(".jsp");
		return resolver;
	}
}
