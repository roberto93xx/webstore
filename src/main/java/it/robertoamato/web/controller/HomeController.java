package it.robertoamato.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.robertoamato.web.config.WebApplicationContextConfig;

@Controller
public class HomeController {

	private static Logger log = LoggerFactory.getLogger(WebApplicationContextConfig.class);

	@RequestMapping(value = "/home",  method = RequestMethod.GET)
	public String welcome(Model model) {
		log.info("welcome - START");
		return "home";
	}
	
}
