package it.robertoamato.web.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import it.robertoamato.web.domain.Product;
import it.robertoamato.web.service.ProductService;

@Controller
public class ProductController {

	public static final Logger log = LoggerFactory.getLogger(ProductController.class);
	
	@Autowired
	private ProductService productService;
	
	@RequestMapping(value = "/products", method = RequestMethod.GET)
	public String getAll(Model model) {
		log.info("getAll - START");
		List<Product> productsList = this.productService.getAll();
		model.addAttribute("productsList", productsList);
		log.info("getAll - END");
		return "productList";
	}
	
	@RequestMapping(value = "/products/{id}", method = RequestMethod.GET)
	public String getById(Model model, @PathVariable String id) {
		log.info("getById - END - id={}", id);
		List<Product> productsList = this.productService.getAll();
		Product product = productsList.get(0);
		model.addAttribute("product", product);
		log.info("getById - END");
		return "productList";
	}
}