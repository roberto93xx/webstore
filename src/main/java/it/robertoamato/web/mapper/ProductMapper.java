package it.robertoamato.web.mapper;


import java.sql.ResultSet;
import java.sql.SQLException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.jdbc.core.RowMapper;

import it.robertoamato.web.domain.Product;

public class ProductMapper implements RowMapper<Product>{

	public static final Logger log = LoggerFactory.getLogger(ProductMapper.class);

	public Product mapRow(ResultSet rs, int rowNum) throws SQLException {
		Product product = new Product();
		product.setName(rs.getString("NAME"));
		product.setDescription(rs.getString("DESCRIPTION"));
		product.setUnitPrice(rs.getBigDecimal("UNIT_PRICE"));
		product.setManufacturer(rs.getString("MANUFACTURER"));
		product.setCategory(rs.getString("CATEGORY"));
		product.setUnitsInStock(rs.getLong("UNITS_IN_STOCK"));
		product.setUnitsInOrder(rs.getLong("UNITS_IN_ORDER"));
		product.setDiscontinued(rs.getBoolean("DISCONTINUED"));
		return product;
	}

}
