package it.robertoamato.web.repository;

import java.util.List;

import it.robertoamato.web.domain.Product;

public interface ProductRepository {

	public List<Product> getAll();

	public Product getById(String id);
}
