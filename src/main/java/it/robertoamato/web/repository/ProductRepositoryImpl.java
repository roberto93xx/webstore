package it.robertoamato.web.repository;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import it.robertoamato.web.domain.Product;
import it.robertoamato.web.mapper.ProductMapper;

@Repository
public class ProductRepositoryImpl implements ProductRepository {

	public static final Logger log = LoggerFactory.getLogger(ProductRepositoryImpl.class);

	@Autowired
	private NamedParameterJdbcTemplate jdbcTemplate;

	public List<Product> getAll(){
		String queryToExecute = "SELECT * FROM PRODUCT";
		Map<String, Object> params = new HashMap<String,Object>();
		List<Product> result = jdbcTemplate.query(queryToExecute, params, new ProductMapper());
		return result;
	}
	

	public Product getById(String id) {
		String queryToExecute = "SELECT * FROM product WHERE ID = productId";
		Map<String, Object> params = new HashMap<String,Object>();
		params.put("productId", id);
		Product result = jdbcTemplate.query(queryToExecute, params, new ProductMapper()).get(0);
		return result;

	}
}