package it.robertoamato.web.service;

import java.util.List;

import it.robertoamato.web.domain.Product;

public interface ProductService {

	public List<Product> getAll();
	
	public Product getById(String id);
}
