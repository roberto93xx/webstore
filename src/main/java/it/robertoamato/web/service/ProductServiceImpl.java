package it.robertoamato.web.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import it.robertoamato.web.domain.Product;
import it.robertoamato.web.repository.ProductRepository;

@Service
public class ProductServiceImpl implements ProductService {

	@Autowired
	private ProductRepository productRepository;
	
	public List<Product> getAll(){
		return this.productRepository.getAll();
	}


	public Product getById(String id) {
		return this.productRepository.getById(id);
	}
}
