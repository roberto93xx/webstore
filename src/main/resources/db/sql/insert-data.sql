INSERT INTO PRODUCT 
	VALUES ('P1234', 'iPhone 6s', 'AppleiPhone 6s smartphone with 4.00-inch 640x1136 display and 8-megapixel rear camera','500','Apple','Smartphone',450,0,false);

INSERT INTO PRODUCT 
	VALUES ('P1235', 'Dell Inspiron','Dell Inspiron 14-inch Laptop (Black) with 3rd Generation Intel Core processors',700,'Dell','Laptop',1000,0,false);

INSERT INTO PRODUCT
	VALUES ('P1236', 'Nexus 7', 'GoogleNexus 7 is the lightest 7 inch tablet With a quad-core Qualcomm Snapdragon™ S4 Pro processor', 300,'Google','Tablet',1000,0,false);